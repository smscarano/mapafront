let api_url ='http://mapaculturalapi.ydns.eu/api/';
let loading = true;
let entidades = [];
let partidos = [];
let clasificaciones = [];
let map;
let markers = [];
let tipos_container_id = "tipos_container";
let partidos_container_id = "partidos_container";

initMap = function(){
    let outside_container = document.createElement("div");
    outside_container.id = "outside_container";

    let map_container = document.createElement("div");
    map_container.classList.add("d-flex");
    map_container.style.height = "400px";
    map_container.width = "100%";
    map_container.id = "map_container";

    outside_container.append(map_container);

    document.body.appendChild(outside_container);
    let buenos_aires = {lat: -35.452781, lng: -60.887374};
    let map = new google.maps.Map(
      document.getElementById('map_container'), {zoom: 6, center: buenos_aires}
    );
    return map;
}

hideMap = function(){
  document.getElementById("outside_container").style.display = "none";
}

showMap = function(){
  if( document.getElementById("outside_container").style.display == "none" )   document.getElementById("outside_container").style.display = "block";
}

createSpinner = function(){
    let spinner_container = document.createElement("div");
    spinner_container.classList.add("d-flex");
    spinner_container.classList.add("justify-content-center");
    spinner_container.id = "spinner_container";

    let spinner_div = document.createElement("div");
    spinner_div.id = "spinner_div";
    spinner_div.classList.add("text-primary");
    spinner_div.innerHTML = '<i class="fas fa-spinner fa-spin"></i>';
    spinner_container.appendChild(spinner_div);
    return spinner_container;
}

destroySpinner = function(){
    let spinner = document.getElementById("spinner_container");
    if(spinner)spinner.remove();
}

createContainer = function(){
  let container = document.createElement("div");
  container.classList.add("container");
  return container;
};

createRow = function(){
  let row = document.createElement("div");             
  row.classList.add("row");
  return row;
}

createCol = function(options){
  let col = document.createElement("div");
  let className = 'col';
  if( options && options.hasOwnProperty("size") )  {
    className += '-' + String ( options["size"] );   
  }
  col.classList.add(className);
  return col;
}

createButton = function(options){
    let button = document.createElement("button");
    button.setAttribute('type', 'button');
    button.classList.add("btn");
    return button;
}

openModal = function(element){
    if( document.getElementById("myModal") ) return;
    hideMap();
    let modal = document.createElement("div");
    modal.id = "myModal";
    modal.classList.add("container");
    modal.classList.add("modal-content");
    
    modal_header = createRow();
    modal_header.classList.add("modal-header");
    let col_header = createCol({size:10});
    console.log(element);
    col_header.innerHTML = '<h1>' + element.denominacion + '</h1>';
    modal_header.append(col_header);

    //modal_header.append(col_left);

    let close_span = document.createElement("span");
    close_span.id = "closeSpan";
    close_span.innerHTML = 'Cerrar';
    //col_right.appendChild(close_span);
    modal_header.appendChild(close_span);

    modal_body = createRow();
    modal_body.classList.add("modal-body");

    modal.append(modal_header);
    modal.append(modal_body);

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    row_modal_container = document.getElementById("row_modal_container");
    row_modal_container.append(modal);

    close_span.addEventListener("click",function(){
        modal.remove();
        showMap();
    });
}

formatDate = function(date_string){
    var d = new Date(date_string);
    let day = String( creation_date_string = d.getDate() );
    if( day.length == 1 ) day = "0" + day;
    let month = String( creation_date_string = d.getMonth() );
    if( month.length == 1 ) month = "0" + month;
    let year = String( creation_date_string = d.getFullYear() );
    let hours = String( creation_date_string = d.getHours() );
    if( hours.length == 1 ) hours = "0" + hours;
    let minutes = String( creation_date_string = d.getMinutes() );
    if( minutes.length == 1 ) minutes = "0" + minutes;
    let date_parsed = day + '/' + month + '/' + year + ' ' + hours + ':' + minutes;
    return date_parsed;
}

createNavBar = function(){
    let nav_div = document.createElement("nav");
    nav_div.id = "navbar";
    nav_div.classList.add("navbar");
    nav_div.classList.add("navbar-expand-sm");
    nav_div.classList.add("bg-primary");

    let ul_elem = document.createElement("ul");
    ul_elem.classList.add("navbar-nav");
    nav_div.appendChild(ul_elem);

    let inicio_li = document.createElement("li");
    inicio_li.classList.add("navbar-item");
    ul_elem.appendChild(inicio_li);

    let inicio_li_a = document.createElement("a");
    inicio_li_a.classList.add("navbar-link");
    inicio_li_a.addEventListener("click", inicio);
    inicio_li_a.id = "inicio_li_a";
    inicio_li_a.href = "#";
    inicio_li_a.innerHTML = '<i class="fas fa-home"></i> Inicio';
    inicio_li.appendChild(inicio_li_a);

    let partidos_li = document.createElement("li");
    partidos_li.classList.add("navbar-item");
    ul_elem.appendChild(partidos_li);

    let partidos_a = document.createElement("a");
    partidos_a.classList.add("navbar-link");
    partidos_a.addEventListener("click",showPartidos);
    partidos_a.href = "#";
    partidos_a.innerHTML = ' <i class="fas fa-map-marker-alt"></i> Partidos';
    partidos_li.appendChild(partidos_a);

    let tipos_li = document.createElement("li");
    tipos_li.classList.add("navbar-item");
    ul_elem.appendChild(tipos_li);

    let tipos_a = document.createElement("a");
    tipos_a.classList.add("navbar-link");
    tipos_a.href = "#";
    tipos_a.addEventListener("click",showTipos);
    tipos_a.innerHTML = '<i class="fas fa-toggle-on"></i> Tipos';
    tipos_li.appendChild(tipos_a);

    document.body.appendChild(nav_div);
}

checkReloadData = function(){
  if( !localStorage.getItem("entidades") || !localStorage.getItem("partidos") || !localStorage.getItem("clasificaciones" ) || !localStorage.getItem("last_connection" ) ){
    return false;
  }

  let last_connection = localStorage.getItem("last_connection" );
  let d = new Date();
  let n = d.getTime();
  let diff = n - last_connection;
  let max = 1000 * 60 * 60 * 6;

  if( diff > max){
    return false;
  }

  return true;
}

loadData = function(){

    if( checkReloadData() ){
      entidades = JSON.parse( localStorage.getItem("entidades") );
      partidos = JSON.parse( localStorage.getItem("partidos") );
      clasificaciones = JSON.parse( localStorage.getItem("clasificaciones") );
      destroySpinner();
      map = initMap();
      addMarkers();
    }
    else{
      let xhr = new XMLHttpRequest();
      xhr.open("GET", api_url + 'entidades', true);
      xhr.setRequestHeader("Accept", "application/json");
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.send();
      xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
              let successful_response = JSON.parse(xhr.response);
              console.log(successful_response);
              entidades = successful_response.entidades;
              partidos = successful_response.partidos;
              clasificaciones = successful_response.clasificaciones;
              localStorage.setItem("entidades", JSON.stringify(entidades));
              localStorage.setItem("partidos", JSON.stringify(partidos));
              localStorage.setItem("clasificaciones", JSON.stringify(clasificaciones));
              
              var d = new Date();
              var n = d.getTime();
              localStorage.setItem("last_connection", n);

              destroySpinner();
              map = initMap();
              addMarkers();
        }
      }
    }
    console.log(entidades);
}

addMarkers = function(){
  for (let index = 0; index < entidades.length; index++) {
    const element = entidades[index];
    var marker = new google.maps.Marker({
      position: {lat:element.lat,lng:element.longitud},
      title: String(element.denominacion)
    });
    marker.addListener("click",function(){
      markerClickHanlder(element)
    });
    markers.push(marker);
  }
  var markerCluster = new MarkerClusterer(map, markers,
    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
}

markerClickHanlder = function(element){
  openModal(element);
}

inicio = function(){
  if(document.getElementById(partidos_container_id)) document.getElementById(partidos_container_id).remove();
  if(document.getElementById(tipos_container_id)) document.getElementById(tipos_container_id).remove();
  if( document.getElementById("outside_container").style.display == "none" ) showMap();
}

showPartidos = function(){
  if(document.getElementById(partidos_container_id)) return false;
  if(document.getElementById(tipos_container_id)) document.getElementById(tipos_container_id).remove();

  hideMap();
  let partidos_container = document.createElement("div");
  partidos_container.id = partidos_container_id;
  partidos_container.classList.add("container");

  //boton cerrar partidos
  let close_partidos = createButton();
  close_partidos.classList.add("btn-primary");
  close_partidos.innerHTML = "Cerrar";
  close_partidos.addEventListener("click",function(){
    document.getElementById(partidos_container.id).remove();
    showMap();
  });
  cerrar_col = createCol({size:"sm"});
  cerrar_col.appendChild(close_partidos);


  //boton mostrar todos partidos
  let mostrar_todos_partidos = createButton();
  mostrar_todos_partidos.classList.add("btn-primary");
  mostrar_todos_partidos.innerHTML = "Seleccionar todos";
  mostrar_todos_partidos.addEventListener("click",function(){
    //document.getElementById(partidos_container.id).remove();
    //showMap();
  });
  todos_col = createCol({size:"sm"});
  todos_col.appendChild(mostrar_todos_partidos);

  //boton mostrar ningun partidos
  let mostrar_ningun_partido = createButton();
  mostrar_ningun_partido.classList.add("btn-primary");
  mostrar_ningun_partido.innerHTML = "Deseleccionar todos";
  mostrar_ningun_partido.addEventListener("click",function(){
      //document.getElementById(partidos_container.id).remove();
      //showMap();
  });
  ninguno_col = createCol({size:"sm"});
  ninguno_col.appendChild(mostrar_ningun_partido);
  
  header_row = createRow();
  header_row.id = "partidos_header_row";
  header_row.classList.add("no-gutters");
  header_row.append(cerrar_col);
  header_row.append(todos_col);
  header_row.append(ninguno_col);

  partidos_container.append(header_row);

  for (let index = 0; index < partidos.length; index++) {
    const partido = partidos[index];
    row = createRow();
    row.innerHTML = partido.descrip;
    row.id = partido.cod_partido;
    row.classList.add("partidos");
    row.classList.add("bg-primary");
    partidos_container.append(row);
  }
  document.body.append(partidos_container);
}

showTipos = function(){
  if(document.getElementById(tipos_container_id)) return false;
  if(document.getElementById(partidos_container_id)) document.getElementById(partidos_container_id).remove();

  hideMap();
  let tipos_container = document.createElement("div");
  tipos_container.id = tipos_container_id;
  tipos_container.classList.add("container");

  //boton cerrar partidos
  let close_tipos = createButton();
  close_tipos.classList.add("btn-primary");
  close_tipos.innerHTML = "Cerrar";
  close_tipos.addEventListener("click",function(){
    document.getElementById(tipos_container_id).remove();
    showMap();
  });
  cerrar_col = createCol({size:"sm"});
  cerrar_col.appendChild(close_tipos);

  //boton mostrar todos partidos
  let mostrar_todos_tipos = createButton();
  mostrar_todos_tipos.classList.add("btn-primary");
  mostrar_todos_tipos.innerHTML = "Seleccionar todos";
  mostrar_todos_tipos.addEventListener("click",function(){
    //document.getElementById(partidos_container.id).remove();
    //showMap();
  });
  mostrar_todos_col = createCol({size:"sm"});
  mostrar_todos_col.appendChild(mostrar_todos_tipos);

  //boton mostrar ningun partidos
  let mostrar_ningun_tipo = createButton();
  mostrar_ningun_tipo.classList.add("btn-primary");
  mostrar_ningun_tipo.innerHTML = "Deseleccionar todos";
  mostrar_ningun_tipo.addEventListener("click",function(){
      //document.getElementById(partidos_container.id).remove();
      //showMap();
  });
  mostrar_ninguno_col = createCol({size:"sm"});
  mostrar_ninguno_col.appendChild(mostrar_ningun_tipo);
  
  header_row = createRow();
  header_row.id = "tipos_header_row";
  header_row.classList.add("no-gutters");
  header_row.append(cerrar_col);
  header_row.append(mostrar_todos_col);
  header_row.append(mostrar_ninguno_col);

  tipos_container.append(header_row);

  for (let index = 0; index < clasificaciones.length; index++) {
    const tipo = clasificaciones[index];
    row = createRow();
    row.innerHTML = tipo;
    row.id = tipo;
    row.classList.add("partidos");
    row.classList.add("bg-primary");
    tipos_container.append(row);
  }
  document.body.append(tipos_container);
}

createModalContainer = function(){
  let row_modal_container = createRow();
  row_modal_container.id = "row_modal_container"
  document.body.append(row_modal_container);
}

createHeader = function(){
  let row_header = createRow();
  row_header.id = "row_header";
  row_header.classList.add("bg-primary");
  let col = createCol();
  col.id = "col_header";

  //col.innerHTML = "<b>test</b>";
  row_header.appendChild(col);
  document.body.append(row_header);
}

onLoadHandler = function(){
    createHeader();
    createNavBar();
    createModalContainer();
    document.body.append( createSpinner());
    loadData();
}
